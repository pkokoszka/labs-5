﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glowny.Contract;
using Wyswietlacz.Contract;

namespace Glowny.Implementation
{
    public class Glowny1 : IGlowny
    {
        private IWyswietlacz wyswietlacz;

        public Glowny1(IWyswietlacz _wyswietlacz)
        {
            this.wyswietlacz = _wyswietlacz;
        }


        public void ZwrocDane(string imie, string nazwisko)
        {
            string dane = imie + " " + nazwisko;
            this.wyswietlacz.UtworzOkno(dane);
        }
    }
}
