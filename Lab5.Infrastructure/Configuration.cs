﻿using System;
using PK.Container;
using Glowny.Contract;
using Glowny.Implementation;
using Wyswietlacz.Contract;
using Wyswietlacz.Implementation;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container kontener = new Container();


            kontener.Register(typeof(Glowny.Implementation.Glowny1));
            kontener.Register(typeof(Wyswietlacz.Implementation.WyswietlaczJeden));

            return kontener;


            
        //todo: Stworzyć dwa komponenty: główny i wyświetlacz. Każdy z komponentów powinien składać się z dwóch bibliotek: kontraktu i implementacji. 
        //todo: Komponent wyświetlacza powinien korzystać z formatki w VisualBasic-u Lab5.DisplayForm. 
        //todo: Sposób użycia przedstawiony jest w metodzie DisplayFormExample w klasie Program w projekcie Lab5.Main. 
        //todo: W projekcie Lab5.Infrastructure należy wypełnić  metodę ConfigureApp w klasie Configuration tak, by stworzyła kontener (zgodny ze specyfikacją z wykładu) i zarejestrowała odpowiednie komponenty.
        }
    }
}
