﻿using Glowny.Contract;
using Glowny.Implementation;
using PK.Container;
using System;
using System.Reflection;
using Wyswietlacz.Implementation;
using Wyswietlacz.Contract;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny1));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(WyswietlaczJeden));

        #endregion
    }
}
